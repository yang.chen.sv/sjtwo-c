#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "gpio.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

static void create_blinky_tasks(void);
static void create_uart_task(void);
static void blink_task(void *params);
static void uart_task(void *params);

static void pin_configure_pwm_channel_as_io_pin(void);
static void pin_configure_adc_channel_as_io_pin(void);

#include "FreeRTOS.h"
#include "pwm1.h"
#include "queue.h"
#include "task.h"

#include "FreeRTOS.h"
#include "task.h"

#include "uart_lab.h"
#define PART3

#if (defined PART0) || (defined PART1)

void uart_read_task(void *p) {
  printf("read task\n");
  char receivedata;

  while (1) {
    // TODO: Use uart_lab__polled_get() function and printf the received value
    uart_lab__polled_get(UART_3, &receivedata);
    printf("Char Recieved %c\n", receivedata);

    // printf(receivedata);
    vTaskDelay(500);
  }
}

void uart_write_task(void *p) {
  printf("write task\n");
  char byte_sent = 'A';

  while (1) {
    // TODO: Use uart_lab__polled_put() function and send a value

    uart_lab__polled_put(UART_3, byte_sent);
    printf("Sent Char %c\n", byte_sent);
    byte_sent += 1;
    if (byte_sent >= 91)
      byte_sent = 'A';

    vTaskDelay(500);
  }
}

#endif
#ifdef PART2

void uart_read_task(void *p) {
  char byte_gotten;
  while (1) {
    if (uart_lab__get_char_from_queue(&byte_gotten, 500))
      fprintf(stderr, "char gotten: %c \n", byte_gotten);
  }
}

void uart_write_task(void *p) {
  // in ascii, character A is 65, Z is 90.
  char byte_sent = 'A';
  while (1) {
    // TODO: Use uart_lab__polled_put() function and send a value
    uart_lab__polled_put(UART_3, byte_sent);
    fprintf(stderr, "char sent: %c \n", byte_sent);
    byte_sent += 1;
    if (byte_sent >= 91)
      byte_sent = 'A';
    vTaskDelay(500);
  }
}

#endif

#ifdef PART3
// This task is done for you, but you should understand what this code is doing
void board_1_sender_task(void *p) {
  char number_as_string[16] = {0};

  while (true) {
    const int number = rand();
    sprintf(number_as_string, "%i", number);

    // Send one char at a time to the other board including terminating NULL char
    for (int i = 0; i <= strlen(number_as_string); i++) {
      uart_lab__polled_put(UART_3, number_as_string[i]);
      printf("Sent: %c\n", number_as_string[i]);
    }

    printf("Sent: %i over UART to the other board\n", number);
    vTaskDelay(3000);
  }
}

void board_2_receiver_task(void *p) {
  char number_as_string[16] = {0};
  int counter = 0;

  while (true) {
    char byte = 0;
    uart_lab__get_char_from_queue(&byte, portMAX_DELAY);
    printf("Received: %c\n", byte);

    // This is the last char, so print the number
    if ('\0' == byte) {
      number_as_string[counter] = '\0';
      counter = 0;
      printf("Received this number from the other board: %s\n", number_as_string);
    }
    // We have not yet received the NULL '\0' char, so buffer the data
    else {
      // TODO: Store data to number_as_string[] array one char at a time
      // Hint: Use counter as an index, and increment it as long as we do not reach max value of 16
      if (counter < 15) // 16th char has to be \0, or else string won't be complete
        number_as_string[counter++] = byte;
    }
  }
}

#endif

void main(void) {

#if (defined PART0) || (defined PART1)

  // TODO: Use uart_lab__init() function and initialize UART2 or UART3 (your choice)
  // TODO: Pin Configure IO pins to perform UART2/UART3 function
  uart_lab__init(UART_3, 96 * 1000 * 1000, 9600);
  // uart_lab__configure_uart_pins(UART_2);
  uart_lab__configure_uart_pins(UART_3);

  xTaskCreate(uart_read_task, "uart_read_task", (512U * 4) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(uart_write_task, "uart_write_task", (512U * 4) / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  vTaskStartScheduler();
#endif
#ifdef PART2

  // Use uart_lab__init() function and initialize UART2 or UART3 (your choice)
  uart_lab__init(UART_3, 96 * 1000 * 1000, 9600);
  // Pin Configure IO pins to perform UART2/UART3 function
  // uart_lab__configure_uart_pins(UART_2);
  uart_lab__configure_uart_pins(UART_3);

  // uart__enable_receive_interrupt(UART_2);
  uart__enable_receive_interrupt(UART_3);

  xTaskCreate(uart_read_task, "uart read task", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(uart_write_task, "uart write task", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  vTaskStartScheduler();

#endif

#ifdef PART3

  // Use uart_lab__init() function and initialize UART2 or UART3 (your choice)
  uart_lab__init(UART_3, 96 * 1000 * 1000, 9600);
  // Pin Configure IO pins to perform UART2/UART3 function
  uart_lab__configure_uart_pins(UART_3);

  uart__enable_receive_interrupt(UART_3);

  xTaskCreate(board_2_receiver_task, "uart receiver task", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(board_1_sender_task, "uart sender task", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler();

#endif
}