#pragma once

#include "uart_lab.h"
/* Baud rate equation from LPC user manual:
 * Baud = PCLK / (16 * (DLM*256 + DLL) * (1 + DIVADD/DIVMUL))
 *
 * What if we eliminate some unknowns to simplify?
 * Baud = PCLK / (16 * (DLM*256 + DLL) * (1 + 0/1))
 * Baud = PCLK / (16 * (DLM*256 + DLL)
 *
 * | DLM | DLL | is nothing but 16-bit number
 * DLM multiplied by 256 is just (DLM << 8)
 *
 * The equation is actually:
 * Baud = PCLK / 16 * (divider_16_bit)
 */
// const uint16_t divider_16_bit = 96*1000*1000 / (16 * baud_rate);
// LPC_UART2->DLM = (divider_16_bit >> 8) & 0xFF;
// LPC_UART2->DLL = (divider_16_bit >> 0) & 0xFF;

void uart_lab__init(uart_number_e uart, uint32_t peripheral_clock, uint32_t baud_rate) {
  // Refer to LPC User manual and setup the register bits correctly
  // The first page of the UART chapter has good instructions
  // a) Power on Peripheral
  printf("uart_lab__init\n");
  LPC_UART_TypeDef *uart_ptr = NULL;
  uint32_t uart_power_bit = NULL;
  if (uart == UART_2) {
    uart_power_bit = 1 << 24;
    uart_ptr = LPC_UART2;

  } else if (uart == UART_3) {
    uart_power_bit = 1 << 25;
    uart_ptr = LPC_UART3;
  }
  LPC_SC->PCONP |= uart_power_bit;
  printf("uart_lab__init sec b\n");

  // b) Setup DLL, DLM, FDR, LCR registers
  const uint32_t character_length_bits = 0x3;  // 8 bit character length
  const uint32_t stop_bit_select_bit = 0 << 2; // 1 stop bit
  const uint32_t parity_enable_bit = 0 << 3;   // parity disable
  const uint32_t divisor_access_bit = 1 << 7;  // enable access to divisor latches

  uart_ptr->LCR = 0; // reset all values
  uart_ptr->LCR = character_length_bits | stop_bit_select_bit | parity_enable_bit | divisor_access_bit;

  const uint16_t divider_16_bit = peripheral_clock / (16 * baud_rate);
  uart_ptr->DLM = (divider_16_bit >> 8) & 0xFF;
  uart_ptr->DLL = (divider_16_bit >> 0) & 0xFF;

  uart_ptr->FDR &= ~(0xF);              // DIVADDRAL = 0 (DEFAULT)
  uart_ptr->FDR |= 1 << 4;              // MULVAL = 1 (DEFAULT)
  uart_ptr->LCR &= ~divisor_access_bit; // disable access to divisor latches in order to enable interrupts
  printf("uart_lab__init sec b end\n");
}

// Read the byte from RBR and actually save it to the pointer
bool uart_lab__polled_get(uart_number_e uart, char *input_byte) {
  // a) Check LSR for Receive Data Ready
  // 0_10, 0_11 001 function U2
  // 0_0, 0_1  010 function U3
  LPC_UART_TypeDef *uart_ptr = NULL;
  const uint8_t receiver_data_ready_bit = 1;

  if (uart == UART_2)
    uart_ptr = LPC_UART2;
  else
    uart_ptr = LPC_UART3;

  // a) Check LSR for Receive Data Ready
  while (!(uart_ptr->LSR & receiver_data_ready_bit)) {
    // delay

  } // else {
  // b) Copy data from RBR register to input_byte
  // printf("read data.. \n");

  *input_byte = uart_ptr->RBR;
  //}

  return true;
}

void uart_lab__configure_uart_pins(uart_number_e uart) {

  if (uart == UART_2) {
    // P0.10 TX , P0.11 RX U2 Read For uart, function is 1
    LPC_IOCON->P0_10 &= ~0x7;
    LPC_IOCON->P0_10 |= 1;
    LPC_IOCON->P0_11 &= ~0x7;
    LPC_IOCON->P0_11 |= 1;
  } else {
    // P4.28 TX. P4.29  RX U3 write. For uart, function is 2
    LPC_IOCON->P4_28 &= ~0x7;
    LPC_IOCON->P4_28 |= 2;
    LPC_IOCON->P4_29 &= ~0x7;
    LPC_IOCON->P4_29 |= 2;
  }
}

bool uart_lab__polled_put(uart_number_e uart, const char output_byte) {
  // a) Check LSR for Transmit Hold Register Empty

  LPC_UART_TypeDef *uart_ptr = NULL;
  const uint8_t send_data_ready_bit = 1 << 5;

  if (uart == UART_2)
    uart_ptr = LPC_UART2;
  else
    uart_ptr = LPC_UART3;
  while (!(uart_ptr->LSR & send_data_ready_bit)) {
    // printf("wait to send data.. \n");
  } // else {
  // b) Copy output_byte to THR register

  uart_ptr->THR = output_byte;
  //}

  return true;
}

// file: uart_lab.c
// TODO: Implement the header file for exposing public functions (non static)

// The idea is that you will use interrupts to input data to FreeRTOS queue
// Then, instead of polling, your tasks can sleep on data that we can read from the queue

// Private queue handle of our uart_lab.c
static QueueHandle_t your_uart_rx_queue;

// Private function of our uart_lab.c
static void your_receive_interrupt(void) {
  // TODO: Read the IIR register to figure out why you got interrupted
  // UART_3
  // Read the IIR register to figure out why you got interrupted
  if (!(LPC_UART3->IIR & 1)) // if at least 1 interrupt is pending (bit 0)
  {
    uint32_t IIR_reg_value = LPC_UART3->IIR;
    uint8_t INTID_value = (IIR_reg_value >> 1) & 0x7;

    // Based on IIR status, read the LSR register to confirm if there is data to be read
    uint32_t receiver_data_available = LPC_UART3->LSR & 0x1;

    if (INTID_value == 0x2 && receiver_data_available) // if receive data available interrupt fires and RDR is not empty
    {
      // TODO: Based on LSR status, read the RBR register and input the data to the RX Queue
      const char byte = LPC_UART3->RBR;
      xQueueSendFromISR(your_uart_rx_queue, &byte, NULL);
    }
  }
  // UART_2
  // Read the IIR register to figure out why you got interrupted
  else if (!(LPC_UART2->IIR & 1)) // if at least 1 interrupt is pending (bit 0)
  {
    uint32_t IIR_reg_value = LPC_UART2->IIR;
    uint8_t INTID_value = (IIR_reg_value >> 1) & 0x7;

    // Based on IIR status, read the LSR register to confirm if there is data to be read
    uint32_t receiver_data_available = LPC_UART2->LSR & 0x1;

    if (INTID_value == 0x2 && receiver_data_available) // if receive data available interrupt fires and RDR is not empty
    {
      // TODO: Based on LSR status, read the RBR register and input the data to the RX Queue
      const char byte = LPC_UART2->RBR;
      xQueueSendFromISR(your_uart_rx_queue, &byte, NULL);
    }
  }
  // TODO: Based on IIR status, read the LSR register to confirm if there is data to be read

  // TODO: Based on LSR status, read the RBR register and input the data to the RX Queue
  // const char byte = UART->RBR;
  // xQueueSendFromISR(your_uart_rx_queue, &byte, NULL);
}

// Public function to enable UART interrupt
// TODO Declare this at the header file
void uart__enable_receive_interrupt(uart_number_e uart_number) {
  // TODO: Use lpc_peripherals.h to attach your interrupt
  LPC_UART_TypeDef *uart_ptr = NULL;

  if (uart_number == UART_2) {
    uart_ptr = LPC_UART2;

    // TODO: Use lpc_peripherals.h to attach your interrupt
    lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__UART2, your_receive_interrupt, NULL);
  }

  else if (uart_number == UART_3) {
    uart_ptr = LPC_UART3;

    // TODO: Use lpc_peripherals.h to attach your interrupt
    lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__UART3, your_receive_interrupt, NULL);
  }

  // TODO: Enable UART receive interrupt by reading the LPC User manual
  // Hint: Read about the IER register
  const uint32_t RBR_interrupt_enable_bit = 1;
  uart_ptr->IER |= RBR_interrupt_enable_bit;

  // TODO: Create your RX queue
  your_uart_rx_queue = xQueueCreate(3, sizeof(char));
}

// Public function to get a char from the queue (this function should work without modification)
// TODO: Declare this at the header file
bool uart_lab__get_char_from_queue(char *input_byte, uint32_t timeout) {

  return xQueueReceive(your_uart_rx_queue, input_byte, timeout);
}